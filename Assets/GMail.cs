﻿using System.Net;
using System.Net.Mail;

public static class GMail
{
	public static void SendEmail
	(
		string from,
		string password,
		string to,
		string subject,
		string body
	)
	{
		var message = new MailMessage
		(
			from: from,
			to: to,
			subject: subject,
			body: body
		);

		var smtp = new SmtpClient
		{
			Host = "smtp.gmail.com",
			Port = 587,
			Credentials = new NetworkCredential(from, password) as ICredentialsByHost,
			EnableSsl = true,
		};

		ServicePointManager.ServerCertificateValidationCallback =
			(sender, certificate, chain, sslPolicyErrors) => true;

		smtp.Send(message);
	}
}