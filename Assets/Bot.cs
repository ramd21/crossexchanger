﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static CrossExchangeAPI;
using Debug = UnityEngine.Debug;

public class Bot : MonoBehaviour
{
	public bool _VerChk;

	public Toggle _TglSell;
	public Toggle _TglBuy;

	public Dropdown _DrpPair;

	public enum Pair
	{
		BTC_USDT,
		ETH_USDT,
		XRP_USDT,
		ADA_USDT,
		ETH_BTC,
		XRP_BTC,
		ADA_BTC,
	}

	Pair _Pair;
	string _coin => _Pair.ToString().Split('_')[0];
	string _baseCoin => _Pair.ToString().Split('_')[1];
	string _pair => _Pair.ToString();
	CoinParam _coinParam => CrossExchangeAPI.GetCoinParam(_pair);


	public InputField _InpSellPrice;
	public InputField _InpPriceDiff;
	public InputField _InpBuyPrice;
	public InputField _InpAmount;
	public InputField _InpCount;
	public InputField _InpKey;
	public InputField _InpSecret;

	public InputField _InpGmail;
	public InputField _InpPass;

	public Text _TxtLog;
	public Text _TxtVer;
	public Text _TxtSysMsg;

	public Text _TxtSell;
	public Text _TxtBuy;
	public Text _TxtPrice;

	public Button _BtnOrder;
	public Button _BtnCancel;
	public Button _BtnError;
	public Button _BtnPrice;
	public Button _BtnSysMsg;

	public Button _BtnPasteSell;
	public Button _BtnPasteBuy;

	public Button _BtnSellUp;
	public Button _BtnSellDown;
	public Button _BtnBuyUp;
	public Button _BtnBuyDown;

	public string _SellId;
	public string _BuyId;

	public Color _SellColor;
	public Color _BuyColor;

	public Camera _Cam;
	public AudioSource _AS;

	string _StrKey;
	string _StrSecret;
	string _StrGmail;
	string _StrPass;

	bool _sellReady =>	!_InpSellPrice.text.IsNullOrEmpty();
	bool _diffReady =>	!_InpPriceDiff.text.IsNullOrEmpty();
	bool _buyReady =>	!_InpBuyPrice.text.IsNullOrEmpty();

	Dictionary<string, string> _ConfigDic = new Dictionary<string, string>();

	IEnumerator CheckConfig()
	{
		Dialog.Open("checking config...");

		UnityWebRequest unityWebRequest = 
		UnityWebRequest.Get("https://docs.google.com/spreadsheets/u/4/d/19rwWQlyczfJZwv3XNti9vEebpR6tUWRdHk6c7oNU-w4/export?format=csv&id=19rwWQlyczfJZwv3XNti9vEebpR6tUWRdHk6c7oNU-w4&gid=0");
		unityWebRequest.SendWebRequest();

		while (true)
		{
			Debug.Log(unityWebRequest.downloadProgress);
			if (unityWebRequest.isDone)
				break;
			yield return null; 
		}

		Debug.Log("done");
		string csv = unityWebRequest.downloadHandler.text;
		string[] lineArr = csv.Split('\n');
		for (int i = 0; i < lineArr.Length; i++)
		{
			Debug.Log(lineArr[i]);
			_ConfigDic.Add(lineArr[i].Split(',')[0], lineArr[i].Split(',')[1]);
		}

		Dialog.Close();
	}

	void Awake()
	{
		Screen.SetResolution(640, 240, false);
		_TxtVer.text = "ver " + Application.version;
		_TxtSysMsg.text = "";
		_Retry = false;

		GetComponentInChildren<Dialog>(true).gameObject.SetActive(true);
	}

	IEnumerator Start()
    {
		yield return CheckConfig();

		bool verChk = true;

#if UNITY_EDITOR
		verChk = _VerChk;
#endif

		if (verChk)
		{
			if (_ConfigDic["ver"].Trim() != Application.version)
			{
				Dialog.Open("new version found ver " + _ConfigDic["ver"] + "\n" + _ConfigDic["download_msg"] + "\n" + _ConfigDic["download"] + "\nclick here to download", () => Application.OpenURL(_ConfigDic["download"]));
				yield break;
			}
		}

		EncryptedPlayerPrefs.keys = new string[]
		{
			"41BDB9EC-CB33-4165-AE31-AF8C8DFC213D",
			"87DDB6C4-B51D-4DAB-B6D2-254E7820A73E",
			"9690E0F9-460A-4F41-BFC3-BDCAF5009EF2",
			"B851A7F4-ABA8-4FE9-943D-1A186EF7EB46"
		};

		_Pair = (Pair)_DrpPair.value;
		PriceUpdater.ActivateSingle(_pair);

		while (true)
		{
			if (PriceUpdater._isPriceInit)
				break;
			yield return null; 
		}

		_DrpPair.onValueChanged.AddListener((i) => 
		{
			_Pair = (Pair)i;
			PriceUpdater.ActivateSingle(_pair);
			Debug.Log(_coinParam._minUnit);

			_InpAmount.text = "";
			_InpSellPrice.text = "";
			_InpBuyPrice.text = "";
			_InpPriceDiff.text = "";
		});

		_BtnError.onClick.AddListener(() =>
		{
			if (!enabled)
			{
				enabled = true;
				_Cam.backgroundColor = Color.black;
				_TxtLog.text = "";
			}
		});

		async void checkDelay()
		{
			await Task.Delay(2000);
			OrderCheck();
		}

		_InpAmount.onEndEdit.AddListener((str) =>
		{
			double amount;
			if (double.TryParse(str, out amount))
			{
				double min = _coinParam._TradeAmountMin;
				double max = min * 30;

				if (amount > max) _InpAmount.text = max.ToString();
				if (amount < min)_InpAmount.text = min.ToString();
			}
		});

		_InpSellPrice.onEndEdit.AddListener((str) =>
		{
			if (str.IsNullOrEmpty()) return;
			if (_diffReady)
			{
				double price = double.Parse(str) - double.Parse(_InpPriceDiff.text);
				_InpBuyPrice.text = price.ToString();
			}

			Reorder();
		});

		_InpPriceDiff.onEndEdit.AddListener((str) =>
		{
			if (str.IsNullOrEmpty()) return;

			if (!_sellReady && !_buyReady)
			{
			}
			else if (_sellReady && _buyReady)
			{
				if (_TglSell.isOn && !_TglBuy.isOn)
				{
					double price = double.Parse(_InpSellPrice.text) - double.Parse(str);
					_InpBuyPrice.text = price.ToString();
				}

				if (!_TglSell.isOn && _TglBuy.isOn)
				{
					double price = double.Parse(_InpBuyPrice.text) + double.Parse(str);
					_InpSellPrice.text = price.ToString();
				}
			}
			else if (_sellReady)
			{
				double price = double.Parse(_InpSellPrice.text) - double.Parse(str);
				_InpBuyPrice.text = price.ToString();
			}
			else
			{
				double price = double.Parse(_InpBuyPrice.text) + double.Parse(str);
				_InpSellPrice.text = price.ToString();
			}
		});

		_InpBuyPrice.onEndEdit.AddListener((str) =>
		{
			if (str.IsNullOrEmpty()) return;
			if (_diffReady)
			{
				double price = double.Parse(str) + double.Parse(_InpPriceDiff.text);
				_InpSellPrice.text = price.ToString();
			}

			Reorder();
		});

		_BtnOrder.onClick.AddListener(() =>
		{
			_SellId = "";
			_BuyId = "";

			Order();
			checkDelay();
		});

		_BtnCancel.onClick.AddListener(() =>
		{
			if (!_SellId.IsNullOrEmpty()) new RestCancelAnOrder(_StrKey, _StrSecret, _pair, _SellId);
			if (!_BuyId.IsNullOrEmpty()) new RestCancelAnOrder(_StrKey, _StrSecret, _pair, _BuyId);
			checkDelay();
		});

		_StrKey = EncryptedPlayerPrefs.GetString("Key", "");
		_StrSecret = EncryptedPlayerPrefs.GetString("Secret", "");
		_StrGmail = EncryptedPlayerPrefs.GetString("Gmail", "");
		_StrPass = EncryptedPlayerPrefs.GetString("Pass", "");

		if (!_StrKey.IsNullOrEmpty()) _InpKey.text = "******************************";
		if (!_StrSecret.IsNullOrEmpty()) _InpSecret.text = "******************************";
		if (!_StrGmail.IsNullOrEmpty()) _InpGmail.text = "******************************";
		if (!_StrPass.IsNullOrEmpty()) _InpPass.text = "******************************";


		_InpKey.onEndEdit.AddListener((str) =>
		{
			_StrKey = str;
			EncryptedPlayerPrefs.SetString("Key", str);
			if (!str.IsNullOrEmpty())
				_InpKey.text = "******************************";
		});

		_InpSecret.onEndEdit.AddListener((str) =>
		{
			_StrSecret = str;
			EncryptedPlayerPrefs.SetString("Secret", str);
			if (!str.IsNullOrEmpty())
				_InpSecret.text = "******************************";
		});

		_InpGmail.onEndEdit.AddListener((str) =>
		{
			_StrGmail = str;
			EncryptedPlayerPrefs.SetString("Gmail", str);
			if (!str.IsNullOrEmpty())
				_InpGmail.text = "******************************";
		});

		_InpPass.onEndEdit.AddListener((str) =>
		{
			_StrPass = str;
			EncryptedPlayerPrefs.SetString("Pass", str);
			if (!str.IsNullOrEmpty())
				_InpPass.text = "******************************";
		});

		_BtnPrice.onClick.AddListener(() =>
		{
			GUIUtility.systemCopyBuffer = _TxtPrice.text;

			_TxtSysMsg.text = "price copied";

			this.WaitForSecond(1f, () =>
			{
				_TxtSysMsg.text = "";
			});
		});

		_BtnPasteSell.onClick.AddListener(() => { _InpSellPrice.text = GUIUtility.systemCopyBuffer; });
		_BtnPasteBuy.onClick.AddListener(() => { _InpBuyPrice.text = GUIUtility.systemCopyBuffer; });
		_BtnSellUp.onClick.AddListener(() =>
		{
			double val;
			if (double.TryParse(_InpSellPrice.text, out val))
			{
				val += _coinParam._minUnit;
				_InpSellPrice.text = _coinParam.PriceFormat(val);
			}
		});
		_BtnSellDown.onClick.AddListener(() =>
		{
			double val;
			if (double.TryParse(_InpSellPrice.text, out val))
			{
				val -= _coinParam._minUnit;
				_InpSellPrice.text = _coinParam.PriceFormat(val);
			}
		});
		_BtnBuyUp.onClick.AddListener(() =>
		{
			double val;
			if (double.TryParse(_InpBuyPrice.text, out val))
			{
				val += _coinParam._minUnit;
				_InpBuyPrice.text = _coinParam.PriceFormat(val);
			}
		});
		_BtnBuyDown.onClick.AddListener(() =>
		{
			double val;
			if (double.TryParse(_InpBuyPrice.text, out val))
			{
				val -= _coinParam._minUnit;
				_InpBuyPrice.text = _coinParam.PriceFormat(val);
			}
		});

		OrderCheckLoop();
	}

	void Update()
	{
		if (!_SellId.IsNullOrEmpty())
		{
			_TxtLog.text = "Selling " + _InpAmount.text + _coin + " at price " + _InpSellPrice.text;
			_Cam.backgroundColor = _SellColor;
		}
		if (!_BuyId.IsNullOrEmpty())
		{
			_TxtLog.text = "Buying " + _InpAmount.text + _coin + " at price " + _InpBuyPrice.text;
			_Cam.backgroundColor = _BuyColor;
		}
		if (_SellId.IsNullOrEmpty() && _BuyId.IsNullOrEmpty())
		{
			_TxtLog.text = "";
			_Cam.backgroundColor = Color.black;
		}

		bool chk =
			!_StrKey.IsNullOrEmpty() &&
			!_StrSecret.IsNullOrEmpty() &&
			!_InpAmount.text.IsNullOrEmpty() &&
			_SellId.IsNullOrEmpty() &&
			_BuyId.IsNullOrEmpty();

		bool chk2 = false;

		if (_TglSell.isOn && !_TglBuy.isOn)
			chk2 = !_InpSellPrice.text.IsNullOrEmpty();

		if (!_TglSell.isOn && _TglBuy.isOn)
			chk2 = !_InpBuyPrice.text.IsNullOrEmpty();

		_BtnOrder.interactable = chk && chk2;
		_BtnCancel.interactable = !_SellId.IsNullOrEmpty() || !_BuyId.IsNullOrEmpty();

		if (_SellId.IsNullOrEmpty())
		{
			if (_TglSell.isOn && !_TglBuy.isOn)
				_TxtSell.color = Color.red;
			else
				_TxtSell.color = Color.white;
		}
		else
		{
			_TxtSell.color = Color.red;
		}

		if (_BuyId.IsNullOrEmpty())
		{
			if (!_TglSell.isOn && _TglBuy.isOn)
				_TxtBuy.color = Color.red;
			else
				_TxtBuy.color = Color.white;
		}
		else
		{ 
			_TxtBuy.color = Color.red;
		}

		_TxtPrice.text = _coinParam.PriceFormat(PriceUpdater.GetPrice(_pair)) + " " + _baseCoin;
	}

	void OnApplicationQuit()
	{
		if (!_SellId.IsNullOrEmpty()) new RestCancelAnOrder(_StrKey, _StrSecret, _pair, _SellId);
		if (!_BuyId.IsNullOrEmpty()) new RestCancelAnOrder(_StrKey, _StrSecret, _pair, _BuyId);
	}

	void OnError(string aError)
	{
		enabled = false;
		_BtnOrder.interactable = false;
		_Cam.backgroundColor = Color.red;
		_TxtLog.text = "Order fail\nClick hear to restart " + aError;
	}

	void Order()
	{
		if (_TglSell.isOn && !_TglBuy.isOn)
		{
			new RestPlaceANewOrder(_StrKey, _StrSecret, false, _coin, _baseCoin, _InpSellPrice.text, "0", _InpAmount.text,
			(res) =>
			{
				_SellId = res.orderid;

				if (!_InpBuyPrice.text.IsNullOrEmpty())
				{
					_OnSellDone = () =>
					{
						new RestPlaceANewOrder(_StrKey, _StrSecret, true, _coin, _baseCoin, _InpBuyPrice.text, "0", _InpAmount.text,
						(res2) =>
						{
							_BuyId = res2.orderid;
							_OnSellDone = null;
						},
						(error) =>
						{
							OnError(error);
						});
					};
				}
			},
			(error) =>
			{
				OnError(error);
			});
		}

		if (!_TglSell.isOn && _TglBuy.isOn)
		{
			new RestPlaceANewOrder(_StrKey, _StrSecret, true, _coin, _baseCoin, _InpBuyPrice.text, "0", _InpAmount.text,
			(res) =>
			{
				_BuyId = res.orderid;

				if (!_InpSellPrice.text.IsNullOrEmpty())
				{
					_OnBuyDone = () =>
					{
						new RestPlaceANewOrder(_StrKey, _StrSecret, false, _coin, _baseCoin, _InpSellPrice.text, "0", _InpAmount.text,
						(res2) =>
						{
							_SellId = res2.orderid;
							_OnBuyDone = null;
						},
						(error) =>
						{
							OnError(error);
						});
					};
				}
			},
			(error) =>
			{
				OnError(error);
			});
		}
	}

	Action _OnSellDone;
	Action _OnBuyDone;

	void Reorder()
	{
		if (!_SellId.IsNullOrEmpty())
		{
			new RestCancelAnOrder(_StrKey, _StrSecret, _pair, _SellId, 
			(res)=> 
			{
				new RestPlaceANewOrder(_StrKey, _StrSecret, res.orderData.isBuy, _coin, _baseCoin, _InpSellPrice.text, "0", res.orderData.remainAmount.ToFixDigitDesimal(1, 8), 
				(res2)=> 
				{
					_SellId = res2.orderid;
				}, 
				(error)=> 
				{
					OnError(error);
				});
			});
		}

		if (!_BuyId.IsNullOrEmpty())
		{
			new RestCancelAnOrder(_StrKey, _StrSecret, _pair, _BuyId, 
			(res)=> 
			{
				new RestPlaceANewOrder(_StrKey, _StrSecret, res.orderData.isBuy, _coin, _baseCoin, _InpBuyPrice.text, "0", res.orderData.remainAmount.ToFixDigitDesimal(1, 8),
				(res2) =>
				{
					_BuyId = res2.orderid;
				},
				(error) =>
				{
					OnError(error);
				});
			});
		}
	}

	void OrderCheck()
	{
		if (!_SellId.IsNullOrEmpty())
		{
			new RestOrderDetail(_StrKey, _StrSecret, _SellId,
			(res) =>
			{
				if (!res.orderData.isActive)
				{
					_SellId = "";
					if (res.orderData.status == OrderDetailData.Status.executed)
					{
						_AS.PlayOneShot(Resources.Load<AudioClip>("sell"));
						string msg = res.orderData.completedAmount + _coin + " sold at " + res.orderData.priceOfTrade;
						if (!_StrGmail.IsNullOrEmpty() && !_StrPass.IsNullOrEmpty())
							GMail.SendEmail(_StrGmail + "@gmail.com", _StrPass, _StrGmail, msg, msg);
						_OnSellDone?.Invoke();
					}
					else
					{
						_OnSellDone = null;
					}
				}
			});
		}

		if (!_BuyId.IsNullOrEmpty())
		{
			new RestOrderDetail(_StrKey, _StrSecret, _BuyId,
			(res) =>
			{
				if (!res.orderData.isActive)
				{
					_BuyId = "";
					if (res.orderData.status == OrderDetailData.Status.executed)
					{
						_AS.PlayOneShot(Resources.Load<AudioClip>("buy"));
						string msg = res.orderData.completedAmount + _coin + " bought at " + res.orderData.priceOfTrade;
						if (!_StrGmail.IsNullOrEmpty() && !_StrPass.IsNullOrEmpty())
							GMail.SendEmail(_StrGmail + "@gmail.com", _StrPass, _StrGmail, msg, msg);
						_OnBuyDone?.Invoke();
					}
					else
					{
						_OnBuyDone = null;
					}
				}
			});
		}
	}

	async void OrderCheckLoop()
	{
		OrderCheck();
		await Task.Delay(1000 * 10);
		OrderCheckLoop();
	}
	
	bool IsDuplicated()
	{
		Process[] proc = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
		if (proc.Length > 1)
			return true;
		return false;
	}
}
