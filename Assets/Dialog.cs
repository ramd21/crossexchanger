﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialog : Singleton<Dialog>
{
	public Text _TxtMsg;
	public Button _BtnMsg;

	Action _OnClick;
	protected override void Awake()
	{
		base.Awake();
		_BtnMsg.onClick.AddListener(()=> 
		{
			_OnClick?.Invoke();
		});
	}

	static public void Open(string aMsg, Action aOnClick = null)
	{
		i.gameObject.SetActive(true);
		i._TxtMsg.text = aMsg;
		i._OnClick = aOnClick;
	}

	static public void Close()
	{
		i.gameObject.SetActive(false);
	}
}
